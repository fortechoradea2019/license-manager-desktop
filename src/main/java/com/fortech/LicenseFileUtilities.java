package com.fortech;



import encrypt.License;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;



public class LicenseFileUtilities {
    License license = new License();

    public void createFile(Path path, String text) {
        try {
            Files.createFile(path);
            Writer writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(path.toString()), "utf-8"));
                writer.write(text);
            } catch (IOException ex) {
                // Report
            } finally {
                try {
                    writer.close();
                } catch (Exception ex) {/*ignore*/}
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public String readFile(Path path) {
        String line = null;
        String fileName = String.valueOf(path.getFileName());
        String response = "";

        try {
            FileReader fileReader =
                    new FileReader(fileName);

            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                response+=line;
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            fileName + "'");
        } catch (IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + fileName + "'");
        }
        return response;
    }

    public boolean isLicenseAvailable(String license) {

        if(this.license.valideSecondToken(license)){
            return true;
        }
        else
        {
            return false;
        }
    }
}

