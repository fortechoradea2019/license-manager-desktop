package com.fortech;




import encrypt.License;
import org.json.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.*;



public class DesktopWebApplication extends JFrame {
    private JButton generateBtn = new JButton("Generate");
    private JTextArea json1Fld = new JTextArea("");
    private JLabel putLbl = new JLabel("Put your validation code here:");
    private JButton pasteValidationKey = new JButton("Paste");
    private JTextArea licenseInput = new JTextArea();
    private JButton validateBtn = new JButton("Validate");


    public License license = new License();

    public DesktopWebApplication() {

        super("Application");
        this.setPreferredSize(new Dimension(500, 300));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);

        Container container = this.getContentPane();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        Color containerColor = Color.decode("#5B9092");
        container.setBackground(containerColor);

        //generateBtn
        generateBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.pack();
        container.add(generateBtn);
        generateBtn.addActionListener(new GenerateBtnEventListener());

        //json1Fld
        json1Fld.setEditable(true);
        json1Fld.setLineWrap(true);
        Color fieldColor = Color.decode("#d0e1e2");
        json1Fld.setBackground(fieldColor);
        container.add(json1Fld);

        //putLbl
        putLbl.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.pack();
        container.add(putLbl);

        //pasteValidationKey
        pasteValidationKey.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.pack();
        container.add(pasteValidationKey);
        pasteValidationKey.addActionListener(new PasteValidationKeyEventListener());

        //licenseInput
        licenseInput.setBackground(fieldColor);
        licenseInput.setEditable(true);
        licenseInput.setLineWrap(true);
        container.add(licenseInput);

        //validateBtn
        validateBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.pack();
        container.add(validateBtn);
        validateBtn.addActionListener(new ValidateBtnEventListener());
        this.pack();

    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }


    class GenerateBtnEventListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            licenseInput.setText("");
            json1Fld.setText(license.getToken());
        }
    }

    class PasteValidationKeyEventListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String dataFromClipboard = "";
            try {
                dataFromClipboard = (String) Toolkit.getDefaultToolkit()
                        .getSystemClipboard().getData(DataFlavor.stringFlavor);
                System.out.println(dataFromClipboard);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            if (dataFromClipboard != "") {
                licenseInput.setText(dataFromClipboard);
            } else JOptionPane.showMessageDialog(null, "Clipboard is empty", "Output", JOptionPane.PLAIN_MESSAGE);
        }

    }

    class ValidateBtnEventListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String message = "";

            if (!licenseInput.getText().equals("")) {
                if(license.valideSecondToken(licenseInput.getText())){
                    message = "License accepted!";
                    String homeDir = System.getProperty("user.dir");

                    Path newFilePath = Paths.get(homeDir + "/license.txt");
                    try {
                        Files.deleteIfExists(newFilePath);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    LicenseFileUtilities licenseFileUtilities = new LicenseFileUtilities();
                    licenseFileUtilities.createFile(newFilePath, licenseInput.getText());
                }
                else{
                    message = "License not accepted!";
                }
            }
            else {
                message = "Empty field";
            }
            JOptionPane.showMessageDialog(null, message, "Output", JOptionPane.PLAIN_MESSAGE);
        }
    }
}
